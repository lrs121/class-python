years = 31
months = 3
days = 12

# Remember, # begins a comment (these comments can be removed)
# This will be simplest with three lines

# The first line calculates age in days
# It'll look sort of like this:  age_in_days = (you get to figure this part out)

# The second should calculate the number of mayfly lifespans age_in_days is

# The third should print out the number of mayfly lifespans

age_in_days = years * 365 + months * 30 + days
life_spans = age_in_days / 4

print ("age in days")
print (age_in_days)
print ("Life spans")
print (life_spans)
