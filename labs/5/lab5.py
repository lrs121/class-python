import turtle
from math import *

class equation:
	#eq = 0
	
	def __init__(self, equation):
		self.eq = equation

	def value(self, x):
		x = x
		return eval(self.eq)
	
	def plot(self, start=-200, end=200):
		turtle.penup()
		x = start
		turtle.pendown()
		turtle.width(5)
		for x in range(start, end):
			turtle.goto(x, eval(self.eq))

wave = equation("sin(x) + cos(2*x)")
print(wave.value(5))
print(wave.value(1))
wave.plot(-5, 5)
riser = equation("x * 10")
print(riser.value(6))
riser.plot(-1, 1)
print(riser.value(5) - wave.value(5))
