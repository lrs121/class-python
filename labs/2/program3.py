import turtle

#used to draw the head
def draw_polygon(sides, sidelength):
        for i in range(sides):
                turtle.forward(sidelength)
                turtle.right(-360.0/sides)

#drawing the body
turtle.right(30)
turtle.forward(60)
turtle.penup()
turtle.right(180)
turtle.forward(60)
turtle.pendown()
turtle.left(60)
turtle.forward(60)
turtle.penup()
turtle.left(180)
turtle.forward(60)
turtle.pendown()
turtle.right(120)
turtle.forward(100)
turtle.right(30)
turtle.forward(60)
turtle.penup()
turtle.left(180)
turtle.forward(60)
turtle.pendown()
turtle.right(120)
turtle.forward(60)
turtle.penup()
turtle.right(180)
turtle.forward(60)
turtle.right(30)
turtle.pendown()
turtle.forward(120)
turtle.right(90)

#draw the head
draw_polygon(20, 12)
