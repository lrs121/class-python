import turtle

#used to draw the head
def drawhead(sides, sidelength):
        for i in range(sides):
                turtle.forward(sidelength)
                turtle.right(-360.0/sides)

#drawing the body
def drawbody(tor, body, limb):
	turtle.right(30)
	turtle.forward(limb)
	turtle.penup()
	turtle.right(180)
	turtle.forward(limb)
	turtle.pendown()
	turtle.left(60)
	turtle.forward(limb)
	turtle.penup()
	turtle.left(180)
	turtle.forward(limb)
	turtle.pendown()
	turtle.right(120)
	turtle.forward(tor)
	turtle.right(30)
	turtle.forward(limb)
	turtle.penup()
	turtle.left(180)
	turtle.forward(limb)
	turtle.pendown()
	turtle.right(120)
	turtle.forward(limb)
	turtle.penup()
	turtle.right(180)
	turtle.forward(limb)
	turtle.right(30)
	turtle.pendown()
	turtle.forward(body)
	turtle.right(90)

#draw the head
scale = int(input('Enter the scale of figure to draw: '))
hdside = 20 * scale
hdlen = 6 * scale
torsz = 100 * scale
bdysz = 120 * scale
limbsz = 60 * scale

drawbody(torsz, bdysz, limbsz)
drawhead(hdside, hdlen)
