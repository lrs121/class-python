from math import *
import turtle

#standard gravity constant
gravk = 6.67e-11

mphk = 2236.9362

#fev stands for find escape velosity
def fev( mass, dist):
	ev = sqrt((2 * gravk * mass) / dist)
	return ev

if __name__ == "__main__":
	obj1_m = float(input('Input the mass of object 1 in kg: '))
	obj1_dkm = float(input('Input the distance from object 1 km: '))
	obj1_d = obj1_dkm * 1000
	obj1_ev = fev(obj1_m, obj1_d)/1000

	obj2_m = float(input('Input the mass of object 2 kg: '))
	obj2_dkm = float(input('Input the distance from object 2 km: '))
	obj2_d = obj2_dkm * 1000
	obj2_ev = fev(obj2_m, obj2_d)/1000

	print('Escape Velocity of object 1 is ' + str(obj1_ev) + 'KM/s or ' + str(obj1_ev * mphk) + ' MPH')
	print('Escape Velocity of object 2 is ' + str(obj2_ev) + 'KM/s or ' + str(obj2_ev * mphk) + 'MPH')

	turtle.left(90)
	turtle.forward(obj1_ev * 10)
	turtle.right(90)
	turtle.forward(30)
	turtle.right(90)
	turtle.forward(obj1_ev * 10)
	turtle.left(90)
	turtle.forward(50)
	turtle.left(90)
	turtle.forward(obj2_ev * 10)
	turtle.right(90)
	turtle.forward(30)
	turtle.right(90)
	turtle.forward(obj2_ev * 10)
	
