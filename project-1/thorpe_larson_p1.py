#import libraries:
import matplotlib.pyplot as plt

#Define funtion for computataion of kw/h cost:
def kwh(watts, cost):
	kw = watts / 1000
	kwhc = kw * cost
	return kwhc

#/if/ loop to define how many objects a being calculated:
#if devices >= 1 or while devices >= 1 or otherway
devices = int(input('Input the nuber devices to calculate '))
while devices > 0:
	try:
		dev_w = float(input('Input the number of watts the device uses'))
	except:
		print('Nothing was entered.')
		break
	try:
		dev_c = float(input('Input the cost of power in $0.00 format'))
	except:
		dev_c = 0.082
	out = kwh(dev_w, dev_c)
	print('The cost of using the device for 10 min is $' + str((10 / 60) * out) + ' the cost for one hour is $' + str(out) + ' and the cost for one day is $' + str(out * 24) + '.')

#exceptions eg catch%try for graceful operations:

#extra-
#pyplot to graph the outputs:
