# Parameters:  Time per guess (number) and number of words in the password
# Returns the amount of time to crack the password, in the same units as
# as time per guess was specified in
def time_to_crack(time_per_guess, number_of_words):
	# Number of words is 99,171
	return time_per_guess * pow(99171, number_of_words)	
