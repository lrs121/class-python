#The puzzle:  Write foo!

def foo(x, y, print_sum=False, return_sum=True):
	if print_sum:
		print("The sum is:  ", x+y)
	if return_sum:
		return x+y

print("This will print a 5:  ", foo(1, 4, return_sum=True))
print("This will print a 10:  ", foo(8, 2, True, True))

