# This makes these numbers easy to change
length = 164.042
width = 20
depth = 10

volume = length * width * depth
weight = 62.4796 * volume # in lbs, provided input was in feet
number_of_horses = weight / 992.08 # in horses (typical)

print("The water in your swimming pool weighs ", number_of_horses, " horses")

# warn if the pool is big
if number_of_horses > 300:
	print("Warning:  Your pool is really big!  I bet you're completely broke!")
elif number_of_horses > 100: 
	print("Warning:  Your pool will take a long time to fill")
	print("Don't use a garden hose")
else:
	print("Warning:  Your pool won't keep your herd alive in the desert")

# warn if the pool is big
# Redundant:  shows nested if
if number_of_horses > 300:	
	print("Warning:  Your pool is really big!  I bet you're completely broke!")
else:
	if number_of_horses > 100: 
		print("Warning:  Your pool will take a long time to fill")
		print("Don't use a garden hose")
	else:
		print("Warning:  Your pool won't keep your herd alive in the desert")
		
