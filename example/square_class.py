import turtle
class square:
	side = 0
	# __init__ is special
	# it will be classed when an instance of the class is created
	def __init__(self, sidelen):
		self.side = sidelen
	
	def area(self):
		return self.side * self.side

	def perimeter(self):
		return 4 * self.side

	def draw(self):
		turtle.forward(self.side)
		turtle.right(90)
		turtle.forward(self.side)
		turtle.right(90)
		turtle.forward(self.side)
		turtle.right(90)
		turtle.forward(self.side)
		turtle.right(90)

window = square(6)
print(window.area())
print(window.perimeter())

wall = square(10)
print(wall.area())
wall.side = 8
print(wall.area())

wall.side = 300
wall.draw()









