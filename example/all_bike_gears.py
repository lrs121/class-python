from bike_gears import *

crank_length = 170
wheel_diameter = 700
chainrings = [38, 50]
sprockets = [30, 26, 22, 18, 16, 14, 13, 12]

for chainring in chainrings:
	for sprocket in sprockets:
		print(chainring, sprocket, difficulty(chainring, sprocket, crank_length, wheel_diameter))
