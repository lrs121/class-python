import turtle
from math import *

def plot_with_eval(f_string, start=-200, end=200, res=1):
	turtle.penup()
	x = start
	turtle.goto(start, eval(f_string))
	turtle.pendown()
	turtle.width(5)
	for x in range(start, end, res):
		turtle.goto(x, eval(f_string))
	
turtle.speed("fastest")
plot_with_eval("sin(x/60) * 200 + cos(x/60) * 200", -600, 600, 10)

def plot_without_eval(f, start=-200, end=200, res=1):
	turtle.penup()
	turtle.goto(start, f(start))
	turtle.pendown()
	turtle.width(5)
	for x in range(start, end, res):
		turtle.goto(x, f(x))

def big_sin(x):
	return sin(x / 60) * 200

def big_cos(x):
	return cos(x / 60) * 300

def composition(x):
	return sin(x/60) * 200 + sin(x/30) * 100 + sin(x/15) * 50 + sin(x/7) * 25 + sin(x/3) * 15

#plot_without_eval(big_sin, -400, 400, res=10)
#plot_without_eval(big_cos)
#plot_without_eval(composition, -600, 600, res=10)
#plot_without_eval(lambda x: 150, -600, 600, res=10)
#plot_without_eval(lambda x: -150, -600, 600, res=10)
input()	
