watts_string = input("How many watts? ")
if watts_string == "":
	print("You didn't use the program right.  Please try again.")
	# you could use an else instead of this
	from sys import exit
	exit()
watts = float(watts_string)
cost_string = input("What is the cost per KW/h? ")
cost = 0.082

# option 1
#if not cost_string == "":
#	cost = float(cost_string)

# option 2
if len(cost_string):
	cost = float(cost_string)

cost_per_hour = (watts / 1000) * cost

print("Cost for 10 minutes:  " +  str(cost_per_hour / 6))
print("Cost for an hour:  " + str(cost_per_hour))
print("Cost for a day:  " + str(cost_per_hour * 24))
