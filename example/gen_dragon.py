#!/usr/bin/python
from math import *

output = "<svg width='1000' height='700' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'> <g stroke='black' stroke-width='1' stroke-linecap='round'>\n"

def drawline(x1, y1, x2, y2):
	global output
	output += '<line x1="' + str(x1) + '" y1="' + str(y1) + '" x2="' + str(x2) + '" y2="' + str(y2) + '" />\n'

def dragline(x1, y1, x2, y2, rem, rev=False):
	l = sqrt((x1-x2)**2 + (y1-y2)**2)
	mx = (x1 + x2)/2
	my = (y1 + y2)/2
	def mkq():
		if y2 < my:
			return -pi/2
		return pi/2
	q = atan( (y2-my) / (x2-mx) ) if not x2 == mx else mkq()
	if x2 < mx:
		q += pi
	
	r = sqrt( (x2-mx)**2 + (y2-my)**2 )
	q -= pi/2
	if rev:
		q += pi
	nx = r*cos(q) + mx
	ny = r*sin(q) + my
	if rem > 1:
		dragline(x1, y1, nx, ny, rem-1,rev=True)
		dragline(nx, ny, x2, y2, rem-1)
	else:
		drawline(x1, y1, nx, ny)
		drawline(nx, ny, x2, y2)

dragline(100, 350, 600, 350, 17)



output +='</g></svg>\n'

open("dragon.svg", "w").write(output)
