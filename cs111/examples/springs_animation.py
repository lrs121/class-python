from __future__ import division, print_function
from visual import *
from time import sleep

display(background=(1, 1, 1))
# For one spring
#ourspring = helix(pos=(0, -5, 0), axis=(0, 10, 0), radius=2, thickness=0.3)
#for i in range(1000):
#	ourspring.length = ourspring.length - 0.01
#	rate(50)

springrows = []
for zpos in range(-30, 31, 5):
	row = []
	for xpos in range(-15, 16, 5):
		row.append(helix(pos=(xpos, -5, zpos), axis=(0, 10, 0), radius=2, thickness=0.3))
	springrows.append(row)

current_row_down = -30
direction = 1
for i in range(10000):
	for row in springrows:
		for spring in row:
			spring.length = spring.length - 0.05 / (abs(spring.z - current_row_down)+1)
			spring.color = (1 - (abs(current_row_down - spring.z)/62), 1, abs(current_row_down - spring.z)/62)
	rate(100)
	current_row_down += 0.1 * direction
	if current_row_down > 30:
		direction = -1
	if current_row_down < -30:
		direction = 1






