def difficulty(teeth_chainring, teeth_sprocket, crank_length, wheel_diameter):
		base_ratio = crank_length / (wheel_diameter / 2)
		gearing_ratio = teeth_chainring / teeth_sprocket
		return base_ratio * gearing_ratio

if __name__ == "__main__":
	cl = float(input("Enter the crank length: "))
	wd = float(input("Enter the wheel diameter:  "))
	cs = float(input("Enter the chainring size:  "))
	ss = float(input("Enter the sprocket size:  "))
	print("The difficulty is:  ", difficulty(cs, ss, cl, wd))


