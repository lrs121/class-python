from pygame import *

init()
screen = display.set_mode((1280,1024))

class Object(object):
	def disp(self):
		screen.blit(self.sprite, self.rect)

class Block(Object):
	def __init__(self):
		self.sprite = image.load("block.bmp")
		self.rect = self.sprite.get_rect()

	def __init__(self, x, y):
		self.sprite = image.load("block.bmp")
		self.rect = self.sprite.get_rect()
		self.rect.centerx = x
		self.rect.centery = y

wall = []
for i in range(20):
	wall.append(Block(100 + 30*i, 300))
for i in range(20):
	wall.append(Block(700 + 30*i, 800))
for i in range(10):
	wall.append(Block(1000 + 30*i, 650))

class Squirrel(Object):
	def __init__(self):
		self.sprite = image.load("squirrel.bmp")
		self.rect = self.sprite.get_rect()
		self.rect.centerx = 100
		self.rect.centery = 100
		self.s_movement = 0
		self.v_movement = 0

	def startright(self):
		self.s_movement = 10

	def startleft(self):
		self.s_movement = -10

	def stop(self):
		self.s_movement = 0

	def jump(self):
		onwall, cb = self.check_wall_collide()
		if onwall:
			self.v_movement += -30

	def check_wall_collide(self):
		onwall = False
		collideblock = 0
		for block in wall:
			if self.rect.colliderect(block.rect):
				onwall = True
				collideblock = block
		return onwall, collideblock

	def update(self): # Expect this to be called during the main loop
		self.rect.y += self.v_movement
		onwall, collideblock = self.check_wall_collide()
		if not onwall:
			self.v_movement += 1
		else:
			self.v_movement = 0
			if collideblock.rect.centery < self.rect.centery:
				self.rect.y = collideblock.rect.centery + 15
			else:
				self.rect.y = collideblock.rect.y - self.rect.height + 1

		self.rect.x += self.s_movement

sq = Squirrel()

while True:
	for e in event.get():
		if e.type == KEYDOWN:
			if e.key == K_RIGHT:
				sq.startright()
			if e.key == K_LEFT:
				sq.startleft()
			if e.key == K_SPACE:
				sq.jump()
		if e.type == KEYUP:
			if e.key == K_RIGHT or e.key == K_LEFT:
				sq.stop()
		if e.type == QUIT:
			print("Ok, time to quit")
			quit()
			from sys import exit
			exit()
			
	screen.fill((255, 255, 255))
	for block in wall:
		block.disp()
	sq.update()
	sq.disp()
	display.flip()
