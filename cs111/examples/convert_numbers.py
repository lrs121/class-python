def convert_number(number_as_string):
	if number_as_string.lower() == "six":
		return 6
	if number_as_string == "five":
		return 5
	if number_as_string == "four":
		return 4
	return int(number_as_string)

