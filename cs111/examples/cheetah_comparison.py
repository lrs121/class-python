# cheetah numbers:
# Top speed: 70 mph or 112 km/h
# Distance at top speed: 500 m or 1,600 ft
cheetah_speed = 112 #km/h
cheetah_distance = 500 #m

# Prey numbers: 
# Top speed: 10 mph
def calculate_headstart(prey_speed):
	cheetah_time = 500 / (112000/(60*60))
	print(cheetah_time)
	prey_distance_ran = prey_speed * cheetah_time
	print("The prey can run " + str(prey_distance_ran) + " meters in the time it takes a cheetah to run 1,600 feet")
	headstart = 500 - prey_distance_ran
	return headstart

if __name__ == "__main__":
	prey_one_speed = float(input("Enter the first prey's top speed (in mph): ")) * .44704
	prey_two_speed = float(input("Enter the second prey's top speed (in mph): ")) * .44704
	headstart1 = calculate_headstart(prey_one_speed)
	headstart2 = calculate_headstart(prey_two_speed)
	print("Prey #1 headstart: " + str(headstart1 * 3.3) + " feet")
	print("Prey #2 headstart: " + str(headstart2 * 3.3) + " feet")
	
	




