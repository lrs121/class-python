import turtle
def draw_graph(a, b, c, scale=1):
	turtle.width(10)
	turtle.left(90)
	turtle.forward(a * scale)
	turtle.backward(a * scale)
	turtle.right(90)
	turtle.forward(50)
	turtle.left(90)
	turtle.forward(b * scale)
	turtle.backward(b * scale)
	turtle.right(90)
	turtle.forward(50)
	turtle.left(90)
	turtle.forward(c * scale)
	turtle.backward(c * scale)
	turtle.right(90)
	turtle.forward(50)
	turtle.left(90)
	
def draw_graph_list(points, scale=1, width=10):
	turtle.width(width)
	turtle.left(90)
	for point in points:
		turtle.forward(point * scale)
		turtle.backward(point * scale)
		turtle.right(90)
		turtle.forward(50)
		turtle.left(90)
	turtle.width(20)

