#include<stdio.h>

int main(){
	int x;
	int y;
	int z;

	printf("The address of x is %ld, y is %ld, z is %ld\n", &x, &y, &z);
	printf("The address of x is %lx, y is %lx, z is %lx\n", &x, &y, &z);

	x = 5;
	printf("x = %d\n", x);
	y = 6;
	printf("y = %d\n", y);
	*(&y + 1) = 17;
	printf("x = %d\n", x);
}
