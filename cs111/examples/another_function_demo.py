def foo(x, y):
	z = 12
	print("Is it working?")
	return y + 10 * x

foo(1, 2)
foo(1, 2)
foo(1, 2)
# print(z) # causes an error because z is not defined here

def handle_input():
	gerbil = float(input("Enter a float number: "))
	return gerbil

our_number = handle_input()
print(our_number)

def print_number_from_global_scope():
	print("This was from outside our scope:  ", our_number)

print_number_from_global_scope()
print(our_number)

def change_number_from_global_scope():
	global our_number
	print("This was from outside our scope:  ", our_number)
	our_number = 1000

change_number_from_global_scope()
print("Our number is now:  ", our_number)
change_number_from_global_scope()


