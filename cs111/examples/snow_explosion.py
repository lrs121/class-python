from visual import *
from time import sleep
from random import random

bigone = sphere(color=(1, 1, 1), radius = 20)
sleep(1)
bigone.visible = false
snowballs = []
trajectories = []
for i in range(100):
	snowballs.append(sphere(color=(1, 1, 1), radius = 0.5))
	trajectories.append((random() - 0.5, random() - 0.5, random() - 0.5))

for repeat in range(10000):
	for i in range(100):
		cx, cy, cz = snowballs[i].pos
		tx, ty, tz = trajectories[i]
		snowballs[i].pos = (cx + tx, cy + ty, cz + tz)
	rate(20)
