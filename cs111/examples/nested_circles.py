from spirograph_color import *

def draw_spiral(number_of_circles, left):
	turtle.width(5)
	for i in range(number_of_circles):
		turtle.color(( (number_of_circles-i)/number_of_circles , 0, i/number_of_circles))
		draw_polygon(36, i)
		turtle.left(left)

if __name__ == "__main__":
	draw_spiral(5, 3)
	turtle.forward(100)
	draw_spiral(20, 20)
