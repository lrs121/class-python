#!/usr/bin/python

from thread import start_new_thread
from time import sleep

def print_stuff_slowly(thing1, thing2):
	while True:
		print(thing1, thing2)
		sleep(1)

start_new_thread(print_stuff_slowly, ("A", "a"))
start_new_thread(print_stuff_slowly, ("B", "b"))
print_stuff_slowly("C", "c")



