# cheetah numbers:
# Top speed: 70 mph or 112 km/h
# Distance at top speed: 500 m or 1,600 ft
cheetah_speed = 112 #km/h
cheetah_distance = 500 #m

# Prey numbers: 
# Top speed: 10 mph

prey_speed = float(input("Enter the prey's top speed (in mph): ")) * .44704

cheetah_time = 500 / (112000/(60*60))
print(cheetah_time)

prey_distance_ran = prey_speed * cheetah_time
print("The prey can run " + str(prey_distance_ran) + " in the time it takes a cheetah to run 1,600 feet")

headstart = 500 - prey_distance_ran
print("The prey can get away with a headstart of " + str(headstart * 3.3) + " feet")



