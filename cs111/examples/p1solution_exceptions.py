while True:
	try:
		watts = float(input("How many watts? "))
		break
	except:
		print("Please enter a number")

cost = 0.082
try:
	cost = float(input("What is the cost per KW/h? "))
except:
	print("Using default value of 0.082")

cost_per_hour = (watts / 1000) * cost

print("Cost for 10 minutes:  " +  str(cost_per_hour / 6))
print("Cost for an hour:  " + str(cost_per_hour))
print("Cost for a day:  " + str(cost_per_hour * 24))
