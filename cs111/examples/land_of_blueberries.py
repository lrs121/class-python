from blueberries import *
from time import sleep
from visual import *
from random import random

def recolor_all(bush, branchcolor, berrycolor):
	try:
		bush[0].color = branchcolor
		bush[1].color = branchcolor
	
		def recolor_bush(bushnum):
			if type(bush[bushnum]) == list:
				recolor_all(bush[bushnum], branchcolor, berrycolor)
			elif type(bush[bushnum]) == sphere:
				bush[bushnum].color = berrycolor
	
		recolor_bush(2)
		recolor_bush(3)
	except:
		print("Bush was too small")

#bush = draw_blueberry_bush((0, 0, 0), 5)
#sleep(1)
#recolor_all(bush, (0.8, 0.7, 0.6), (0.5, 0.1, 1.0))
def draw_row(length, centerpoint):
	cx, cy, cz = centerpoint
	bushes = range(length)
	for i in range(length):
		if random() > 0.5:
			bushes[i] = draw_blueberry_bush( ((i-length//2)*50 + cx, cy, cz), 7)
		else:
			bushes[i] = draw_tree( ((i-length//2)*50+cx, cy, cz), 7)
	return bushes

rows = [draw_row(11, (0, 0, (i - 5) * 50)) for i in range(11)]

for row in rows:
	for b in row:
		recolor_all(b, (0.8, 0.7, 0.6), (0.5, 0.1, 1.0))
		sleep(0.1)


