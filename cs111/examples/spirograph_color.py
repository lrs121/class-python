import turtle

def draw_polygon(sides, sidelength):
	for i in range(sides):
		turtle.forward(sidelength)
		turtle.right(360.0/sides)


if __name__ == "__main__":
	turtle.width(5)
	
	for i in range(36):
	        turtle.color((i/36.0, 0, 1 - (i/36.0)))
	        draw_polygon(30, 36)
	        turtle.right(10)
	
