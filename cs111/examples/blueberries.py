from __future__ import division, print_function
from visual import *
from random import random
from time import sleep
from thread import start_new_thread

def draw_tree(pos, max_divs):
	if max_divs < 1 or random() / max_divs > 0.2:
		# draw a blueberry
		return sphere(pos=pos, color=(0, 1, 0), radius=2)
	else:
		sx, sy, sz = pos
		def rand_in_range():
			return (0.5 - random()) * max_divs * 5
		# separate into two branches
		new_pos_1 = (sx + rand_in_range(), sy + max_divs*3 + rand_in_range(), sz + rand_in_range())
		new_pos_2 = (sx + rand_in_range(), sy + max_divs*3 + rand_in_range(), sz + rand_in_range())
		return [curve(pos=[pos, new_pos_1], color=(0.8, 0.5, 0.5), radius=max_divs/5),
					curve(pos=[pos, new_pos_2], color=(0.8, 0.5, 0.5), radius=max_divs/5),
					draw_tree(new_pos_1, max_divs-1),	draw_tree(new_pos_2, max_divs-1)]

def draw_blueberry_bush(pos, max_divs):
	if random() > 0.8 or max_divs == 0:
		# draw a blueberry
		return sphere(pos=pos, color=(0, 0, 1), radius=1)
	else:
#		sleep(0.5)
		sx, sy, sz = pos
		def rand_in_range():
			return (0.5 - random()) * 24
		# separate into two branches
		new_pos_1 = (sx + rand_in_range(), sy + 10 + rand_in_range(), sz + rand_in_range())
		new_pos_2 = (sx + rand_in_range(), sy + 10 + rand_in_range(), sz + rand_in_range())
		return [curve(pos=[pos, new_pos_1], color=(0, 1, 0), radius=0.5),
					curve(pos=[pos, new_pos_2], color=(0, 1, 0), radius=0.5),
					draw_blueberry_bush(new_pos_1, max_divs-1),	draw_blueberry_bush(new_pos_2, max_divs-1)]

def pause():
    while True:
        rate(30)
        if scene.kb.keys:
            k = scene.kb.getkey()
            return k

rotating = True
def start_and_stop_rotating():
	global rotating
	while True:
		key = pause()
		if key == 'r':
			rotating = True
		if key == 'h':
			rotating = False

if __name__ == "__main__":
	draw_tree(pos=(0, -50, 0), max_divs=10)
	scene.background=(1, 1, 1)
	start_new_thread(start_and_stop_rotating, tuple())
	for t in range(100000):
		if rotating:
			scene.forward = scene.forward.rotate(axis=(1, 1, 1), angle=6.28/360)	
		sleep(1/60)
