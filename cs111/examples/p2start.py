#!/usr/bin/python3
# The above line makes the program able to run as a normal program on Linux
# You can take it out if you like
# If the program is set executable, it can be run with:
# ./p2start.py
# If the result is permission denied, run:
# chmod 700 p2start.py
# chmod only needs to be run once.  The line above sets the file permissions
# to allow execution.  That's how the operating system determines if a file 
# is a program, or something else like a document, text file, etc.

from math import *

# Write your functions here


# Don't change anything below this line
if __name__ == "__main__":
	print("This should be 125: ", cube(5))
	print("This should be 20: ", area_of_right_triangle(4, 10))
	print("This should be 20943.95:", volume_of_cone(10, 200))
	print("This should be 262206:", weight_of_water_room(20, 30, 7))

	# Small house, decent size rabbit (New Zealand, etc)
	print("This should be 100:", rabbit_house(1000, 10, "light"))
	# Apartment, smaller rabbits (dwarf varieties, like fuzzy lop, etc), high density
	print("This should be 166:", rabbit_house(400, 4, "dense"))
	# Big house, decent size rabbit (champagne d'argent, etc)
	print("This should be 533:", rabbit_house(3600, 9, "normal"))

	print("This should count down through all the numbers:  ",)
	print([number_string(i)for i in range(10)])

	def fun_with_numbers(x):
		o = x % 10
		x -= o
		t = (x % 100)
		x -= t
		h = x
		print(number_string(int(h/100)) + " hundered, " + number_string(int(t/10)) + " tens, and " + number_string(int(o)))
	# You can guess what these do easy enough, just make sure the output looks right
	fun_with_numbers(357)
	fun_with_numbers(243)





