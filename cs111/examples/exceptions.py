
try:
	int("apple")
except:
	print("We're in except")

print("We're still going")

# multi-line comment hack
'''
	This is a string
'''
# User input, but they have to get it right!
while True:
	try:
		instring = input("Enter a number:  ")
		input_number = int(instring)
		break
	except:
		print("That wasn't a number.")

# general form:
# while True:
#	collect input
#	if the input is ok:
#		break

# Enter the password
while True:
	instring = input("Enter the magic word:  ")
	if instring == "fish":
		break
	elif instring == "gerbil":
		break
	else:
		print("That wasn't a magic word")

print("Good job!  You entered the number " + str(input_number))

while True:
	instring = input("Enter the magic word or a number:  ")
	if instring == "fish":
		break
	elif instring == "gerbil":
		break
	else:
		print("That wasn't a magic word")
	try:
		input_number = int(instring)
		break
	except:
		print("That wasn't a number.")
print("You're free!")
