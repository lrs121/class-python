# Prints out a spiral with a turtle
# Version 1.0
# 1/21/2015

import turtle
distance = 200
side = 0
while True:
	if not side:
		turtle.forward(distance+5)
	elif side == 3:
		turtle.forward(distance+10)
	else:	
		turtle.forward(distance)
	turtle.right(90)
	distance = distance + 3
# Warning:  The following line causes a crash
# when distance > 500
	turtle.color((distance/500.0, 0, 0.5))
	side += 1
	if side > 3:
		side = 0
		

