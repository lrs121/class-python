def fact(x):
	if x == 1:
		return 1
	return fact(x-1) * x


print("Factorial of 5:  ", fact(5))
print("Factorial of 11:  ", fact(11))
print("Factorial of 100:  ", fact(100))
print("Factorial of 997:  ", fact(997))
