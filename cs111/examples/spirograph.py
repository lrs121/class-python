import turtle

def draw_polygon(sides, sidelength):
	for i in range(sides):
		turtle.forward(sidelength)
		turtle.right(360.0/sides)

for i in range(36):
	draw_polygon(30, 36)
	turtle.right(10)
	turtle.color((i/36.0, i/36.0, 0))
