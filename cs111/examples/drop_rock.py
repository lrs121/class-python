from visual import *
from random import random
display(background=(1, 1, 1))

ramp_positions = [(0, 0, 0), (2, 0, 0), (0, 1, -1), (2, 0, 0), (2, 1, -1), (0, 1, -1)]
faces(pos=ramp_positions, color=[(1, 1, 1), (0, 1, 0), (0, 0, 1)])

rocks = []
rvs = []
for i in range(100):
	rocks.append(sphere(pos =(random(), 1.5 + random(), 2 + random()), radius = .1))
	rvs.append([(random()-0.5)/100, -0.005 + random()/1000, -.01])

def detect_collision(r):
	rx, ry, rz = r.pos
# 	ramp_positions can be used to allow moving the ramp around, later
#	collision plain is from 0-1 on the X axis to top end of the ramp at 0, 1, 1 to 1, 1, 1
	if rx > 2 or rx < -1:
		return 0
	if ry > 1 or ry < -1:
		return 0
	if rz < -1 or rz > 1:
		return 0
	if ry + 0.1 > -rz and ry - 0.1 < -rz:
		return 1


for t in range(1000):
	for rock, rv in zip(rocks, rvs):
		rx, ry, rz = rock.pos
		rock.pos = (rx + rv[0], ry + rv[1], rz + rv[2])
		if detect_collision(rock):
			tmp = rv[1]
			rv[1] = -rv[2] 
			rv[2] = -tmp 
	rate(60)


