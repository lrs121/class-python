def fact(x):
	if x == 1:
		return 1
	return fact(x-1) * x

function_in_jar = fact
print(function_in_jar(10))

def run_function_on_6(f):
	return f(6)

print(run_function_on_6(function_in_jar))

def return_print():
	return print

retval = return_print()
retval("hi")

rp_in_var = return_print
rp_in_var()("test")

def return_adder():
	def adder(a, b):
		return a + b
	return adder

print(return_adder)
print(return_adder())
print(return_adder()(2, 4))

count = 0
def counter():
	global count
	count += 1
	return count

print(counter())
print(counter())
print(counter())
print(counter())
print(counter())

# Won't work!  Stack frame is removed once return_new_counter is done!
#def return_new_counter():
#	inside_count = 0
#	def inside_counter():
#		global inside_count
#		inside_count += 1
#		return inside_count
#	return inside_counter
#
#counter1 = return_new_counter()
#print(counter1())

var_with_function = lambda x, y: x * 10 + y
print(var_with_function(4, 2))

var_with_function = lambda x : print("x = ", x)
var_with_function("ten")


